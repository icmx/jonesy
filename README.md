# Jonesy

Jonesy is an extremely minimalistic aggregator for RSS, Atom and other web feeds. It works between reader application and original feed source which allows to share and synchronise the single feeds list by multiple devices and save original reader application experience.

One should consider that Jonesy is more of an idea than real software which is actually pretty crappy.

![How Jonesy Works (Sequence Diagram)](doc/how-jonesy-works.png "How Jonesy Works (Sequence Diagram)")

## Setup

Jonesy depends only on Python 3 standard components. You just have to  obtain the code, manually or by using git:

```sh
git clone "https://gitlab.com/icmx/jonesy" "jonesy-local"
```

Then run Jonesy in setup mode:

```sh
cd jonesy-local/bin
./jonesy setup
```

It will create necessary directories hierarchy and copy the example feeds list. If [*$XDG_CONFIG_HOME*](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html) environment variable is defined, then setup will use it to create Jonesy home like *~/.config/jonesy*, otherwise *~/.jonesy* will be used. One can also set up completely custom home by using *$JONESY_HOME* (see [below](#environment-variables)).

Optionally, one can also put *jonesy-local/bin* absolute path to *$PATH* variable.

## Usage

### Modes

One can run Jonesy in these modes:

  - `jonesy setup` — single-shot, generates Jonesy home directory and example config
  - `jonesy fetch` — single-shot, obtains feeds from sources defined in config file
  - `jonesy serve` — runs until cancelled, this mode serves retrieved feeds for external reader application (127.0.0.1 port 8600 by default).
  - `jonesy multi` — runs until cancelled, combination of *serve* mode with *fetch*ing in specified interval (30 minutes by default).

### External Readers Connection

To use Jonesy, one should connect an external application to Jonesy's feeds. Suppose one have the following feed in config file:

```xml
<feed source="http://example.org/rss" result="news.feed" />
```

By default reader application connects to original feed URL — http://example.org/rss, but since Jonesy retrieves and serves feeds locally, reader should use local feed link instead — http://127.0.0.1:8600/feeds/news.feed. Note the path: `news.feed` is actually result file previously specified in config.

Replace 127.0.0.1 by external address, e.g. http://example.org:8600/feeds/news.feed if you run Jonesy on external host (like on home server).

### Updating Local Feeds

One can update Jonesy local feeds automatically by usung *multi* mode.

There are manual options also: *fetch* mode should be launched from time to time by hand or as a recurrent job by *at*, *Cron*, *systemd* or other suitable scheduler for your system.

Another option is special */fetch* URL:

> http://127.0.0.1:8600/fetch

Or, if Jonesy is serving on another host:

> http://example.org:8600/fetch

Access to that URL will signal Jonesy to start local feeds refreshing. It's might be helpful e.g. if you need to force update from another device with no direct access to Jonesy.

## Configuration

Jonesy is a simple application and thus relies only on environment variables and feeds list defined in XML file.

### Environment Variables

  - `$JONESY_HOME` — directory for feeds list and retrieved items. If `$XDG_CONFIG_HOME` is defined, then it will be assumed as `$XDG_CONFIG_HOME/jonesy`, or `~/.jonesy` otherwise.
  - `$JONESY_HOST` — address on which Jonesy will host local feeds, 127.0.0.1 by default.
  - `$JONESY_PORT` — port, 8600 by default
  - `$JONESY_USERAGENT` — "Browser" user agent string. By default Jonesy is trying to disguise itself as a Firefox, so feeds web servers will treat it as a human user.
  - `$JONESY_THREADS` — number of threads which will fetch feed items, 4 by default.
  - `$JONESY_FORMAT` — parsing format, support only these values:
    - `muon`: parse Muon feeds file (*feeds.muon*) — default
    - `opml`: parse OPML subscriptions flat list (*feeds.opml*)
  - `$JONESY_TIMEOUT` — timeout for single feed fetching in a seconds. Feed refreshing will be aborted after that time. Default value is 300, i.e. 5 minutes.
  - `$JONESY_INTERVAL` — interval in which Jonesy will update local feeds if running in *multi* mode. Default value is 1800, i.e. 30 minutes.

### Files and Directories

  - `$JONESY_HOME/feeds` — directory for retrieved feeds files.
  - `$JONESY_HOME/feeds.$JONESY_FORMAT` — file for feeds list, depends on `$JONESY_FORMAT` defined. By default it's *feeds.muon*.

*Notes:*

  - Muon specification is currently in early development and available [here](https://gitlab.com/icmx/muon).
  - Classic OPML specification is available [here](http://dev.opml.org/spec2.html)

#### Note for Ampersand Characters

Some feeds URLs contains ampersand characters `&`, for instance:

```
http://example.org/get?news&type=rss"
                           ^ this
```

Your feeds list (as well as other XML files) should avoid ampersands and replace them by special escape sequence `&amp;`, like so:

```xml
<feed source="http://example.org/get?news&amp;type=rss" result="news.feed" />
<!--                                     ^^^^^ this                       -->
```

## TODO

### For Current

  - [x] ~~Add first-launch self-configuration~~ *setup* mode added
  - [x] Rename `$JONESY_MODE` to `JONESY_FORMAT`
  - [x] ~~Show `title` value in log if available, `source` if not~~ source is better for logging purpose
  - [ ] Support `title` attribute in `<feed>` element (Muon, OPML)
  - [ ] Add optional feeds listing
  - [ ] Add 404 page (for not existing feeds e.g.)
  - [ ] Rewrite dirty sections
  - [ ] Fix HUGE memory leak
  - [ ] Move to the Jonesy NG version

### For Jonesy NG

  - [ ] Make it socket-based
  - [ ] Add basic sanitizer functions (e.g. image to base64)
  - [ ] Add basic web-interface
    - [ ] Static page
    - [ ] Javascript JSON parsers and page builders
  - [ ] Design basic API
